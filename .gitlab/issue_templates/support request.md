*Please check if our www.cardbook.icu/FAQ or www.cardbook.icu/issues solve your problem.*

<!-- If not, please describe it here (in English).
Fill in ALL relevant sections below and delete inapplicable ones.
Screenshots and log files may be helpful (see below). -->


~5981411

## summary


## screenshot


## steps to reproduce the problem
1. 
2. 
3. 

## current behavior
(what actually happens following the steps above)


## expected behavior
(what you should see instead)


## what I have tried so far to solve the problem
1. 
2. 
3. 

## my version numbers
* CardBook: 
* Thunderbird: 
* OS: 


## CardBook log
````````````````````````````````````````````````
CardBook tab > status bar > click on log entry
increase log size: www.cardbook.icu/FAQ#4
* copy/paste the log here *
````````````````````````````````````````````````


## Thunderbird console log
```````````````````````````
press CTRL+SHIFT+J
* copy/paste the log here *
```````````````````````````



/label ~5981411